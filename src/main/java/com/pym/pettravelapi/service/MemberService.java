package com.pym.pettravelapi.service;

import com.pym.pettravelapi.entity.Member;
import com.pym.pettravelapi.model.member.MemberCreateRequest;
import com.pym.pettravelapi.model.member.MemberInfoChangeRequest;
import com.pym.pettravelapi.model.member.MemberItem;
import com.pym.pettravelapi.model.member.MemberResponse;
import com.pym.pettravelapi.repository.MemberRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.BlockingDeque;

@Service
@RequiredArgsConstructor
public class MemberService {
    private final MemberRepository memberRepository;

    public Member getData(long id){
        return memberRepository.findById(id).orElseThrow();
    }

    public void setMember (MemberCreateRequest request){
        Member addData = new Member();
        addData.setMemberName(request.getMemberName());
        addData.setPhoneNumber(request.getPhoneNumber());

        memberRepository.save(addData);
    }

    public List<MemberItem> getMembers(){
        List<Member> originList = memberRepository.findAll();

        List<MemberItem> result = new LinkedList<>();

        for (Member member: originList){
            MemberItem addItem = new MemberItem();
            addItem.setId(member.getId());
            addItem.setMemberName(member.getMemberName());
            addItem.setPhoneNumber(member.getPhoneNumber());

            result.add(addItem);
        }
        return result;
    }

    public MemberResponse getMember(@PathVariable long id){
        Member originData = memberRepository.findById(id).orElseThrow();

        MemberResponse response = new MemberResponse();
        response.setId(originData.getId());
        response.setMemberName(originData.getMemberName());
        response.setPhoneNumber(originData.getPhoneNumber());

        return response;
    }

    public void putMember(long id, MemberInfoChangeRequest request){
        Member originData = memberRepository.findById(id).orElseThrow();
        originData.setMemberName(request.getMemberName());
        originData.setPhoneNumber(request.getPhoneNumber());

        memberRepository.save(originData);
    }
}
