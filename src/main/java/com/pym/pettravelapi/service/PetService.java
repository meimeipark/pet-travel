package com.pym.pettravelapi.service;

import com.pym.pettravelapi.entity.Member;
import com.pym.pettravelapi.entity.Pet;
import com.pym.pettravelapi.model.pet.PetCreateRequest;
import com.pym.pettravelapi.repository.PetRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class PetService {
    private final PetRepository petRepository;

    public void setPet(Member member, PetCreateRequest request){
        Pet addData = new Pet();
        addData.setPetName(request.getPetName());
        addData.setMember(member);
        addData.setPetType(request.getPetType());

        petRepository.save(addData);
    }
}
