package com.pym.pettravelapi.model.member;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class MemberResponse {
    private Long id;
    private String memberName;
    private String phoneNumber;
}
