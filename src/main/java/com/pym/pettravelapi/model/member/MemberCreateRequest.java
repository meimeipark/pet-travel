package com.pym.pettravelapi.model.member;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class MemberCreateRequest {
    private String memberName;
    private String phoneNumber;
}
