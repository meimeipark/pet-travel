package com.pym.pettravelapi.model.pet;

import com.pym.pettravelapi.entity.Member;
import com.pym.pettravelapi.enums.PetType;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PetCreateRequest {
    private String petName;

    @Enumerated(value = EnumType.STRING)
    private PetType petType;
}
