package com.pym.pettravelapi.configure;

import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.info.Info;
import lombok.RequiredArgsConstructor;
import org.springdoc.core.models.GroupedOpenApi;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@OpenAPIDefinition(
        info = @Info(title = "pet travel App",
                description = "pet travel app api 명세",
                version = "v1")
)
@RequiredArgsConstructor
@Configuration
public class SwaggerConfig {

    @Bean
    public GroupedOpenApi v1OpenApi(){
        String[] paths = {"/v1/**"};

        return GroupedOpenApi.builder()
                .group("반려동물들과의 정기 모임 API v1")
                .pathsToMatch(paths)
                .build();
    }
}
