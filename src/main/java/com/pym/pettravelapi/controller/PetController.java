package com.pym.pettravelapi.controller;

import com.pym.pettravelapi.entity.Member;
import com.pym.pettravelapi.model.pet.PetCreateRequest;
import com.pym.pettravelapi.service.MemberService;
import com.pym.pettravelapi.service.PetService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/pet")
public class PetController {
    private final MemberService memberService;
    private final PetService petService;

    @PostMapping("/new/member-id/{memberId}")
    public String setPet(@PathVariable long memberId, @RequestBody PetCreateRequest request){
        Member member = memberService.getData(memberId);
        petService.setPet(member, request);

        return "반려동물 등록";
    }
}
