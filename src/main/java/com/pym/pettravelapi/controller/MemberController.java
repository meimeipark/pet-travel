package com.pym.pettravelapi.controller;

import com.pym.pettravelapi.model.member.MemberCreateRequest;
import com.pym.pettravelapi.model.member.MemberInfoChangeRequest;
import com.pym.pettravelapi.model.member.MemberItem;
import com.pym.pettravelapi.model.member.MemberResponse;
import com.pym.pettravelapi.service.MemberService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/member")
public class MemberController {
    private final MemberService memberService;

    @PostMapping("/join")
    public String setMember (@RequestBody MemberCreateRequest request){
        memberService.setMember(request);

        return "회원 등록";
    }

    @GetMapping("/list")
    public List<MemberItem> getMembers(){
        return memberService.getMembers();
    }

    @GetMapping("/detail/{id}")
    public MemberResponse getMember(@PathVariable long id){
        return memberService.getMember(id);
    }

    @PutMapping("/info/{id}")
    public String putMember(@PathVariable long id, @RequestBody MemberInfoChangeRequest request){
        memberService.putMember(id, request);

        return "정보 수정";
    }
}
