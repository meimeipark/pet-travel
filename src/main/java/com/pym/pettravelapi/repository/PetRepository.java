package com.pym.pettravelapi.repository;

import com.pym.pettravelapi.entity.Pet;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PetRepository extends JpaRepository<Pet, Long> {
}
